<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 11/28/2015
 * Time: 12:03 PM
 */

namespace Services\AccountService;

use App\Account;
use App\User;


/**
 * Class CreateAccountService
 * @package Services
 */
class CreateAccountService
{

    /**
     * CreateAccountService constructor.
     * @param Account $source
     * @param User       $user
     */
    public function __construct(Account $source, User $user)
    {
        $this->source = $source;
        $this->user   = $user;
    }

    /**
     * @param Array $fields
     */
    public function create(array $fields)
    {
        $create = new Account;

        $create->username = $fields->username;
        $create->password = $fields->hostname;
        $create->plan     = $fields->plan;

        if($create->save())
        {
            $this->user->account()->attach($create->id);
        }

    }

    /**
     * @param array $fields
     * return array
     */
    public function updateAccount(array $fields)
    {
        $update = Account::findOrFail($fields->id);

        foreach($fields as $key => $value) {
            $update->$key = $value;
        }
        if($update->save())
            return [
                "Everything saved" => '200',
                ];
    }

}