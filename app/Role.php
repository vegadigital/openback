<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @package App
 */
class Role extends Model
{
    //
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function user()
    {
        return $this->belongsToMany('App\User', 'account_user_role')->withPivot(['account_id','user_id','role_id']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function account()
    {
        return $this->belongsToMany('App\Account', 'account_user_role')->withPivot(['account_id','user_id','role_id']);
    }
}
