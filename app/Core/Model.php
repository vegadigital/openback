<?php
/**
 * Created by PhpStorm.
 * User: tim
 * Date: 11/29/2015
 * Time: 8:41 AM
 */

namespace Core;
use Illuminate\Database\Eloquent\Model as Main;

class Model extends Main
{
    public function Guid()
    {
        if (function_exists('com_create_guid') === true) {
            return trim(com_create_guid(), '{}');
        }
        $server = $_SERVER['SERVER_NAME'];
        $hostname = explode('.',$server);
        $server = $hostname[0];
        return printf("uniqid($server): %s\r\n", uniqid($server.'_'));
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

}