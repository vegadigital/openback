<?php 
return [
	/*
	|--------------------------------------------------------------------------
	| Host of your server
	|--------------------------------------------------------------------------
	|
	| Please provide this by its full URL including its protocol and its port
	|
	*/
	'host'=>'https://166.70.63.10:2087',

	/*
	|--------------------------------------------------------------------------
	| Remote Access key
	|--------------------------------------------------------------------------
	|
	| You can find this remote access key on your CPanel/WHM server. 
	| Log in to your server using root, and find `Remote Access Key`.
	| Copy and paste all of the string
	|
	*/
	'auth'=>'947057ff1189d109bdc3e1eabb9a7e1551cab49dfb624732df20445109d07a9134f12487a66cbdfc15ae05fdb903225bf3f03d80d3660a3c0ea3064abcc2fc9b5ffaac7f3a30234b476f5a697100b8a1d9dbf730d29eecc17fcdce4a78360b98d52d0ca1cc66fe720ced7820e4d9cadb9bb43d31edfb1f5b000a68579eb22629cd540555002ceec686f09a4579f6d80d2feb9dc1a2360391cbc944e6ebe3d3100112261d12e02a557273c8550f175bec3edf6c93f54a849548d04322bff5871a7d40c4ccfe2dd18777e763d5329399b88d6556a47c0c2677572f6242004c757361dfbf7f98affd5e3ec013d116c7affa9c573e56dd7200f1bd78f586e7c8d9c100ed5fb6f6ad329a3e7b70142f3f42568a8cedba0cb68e5a3bbd86e7f3b4c0057e435063036595919e61e6f0beccd9796b558f97069d4a375674ff709ea791cd2f5a25e78bd3eaa2bf4c352a22d04b8b8d7db0e6112a6166f9ba68881cec37751d21010951706b3e3ffc933cb6cf41edc9fe67fb5e5fc797603be57c7e3849292e6a468cf51eb00b002f114b1c4248a910122ce4868bf9cc2a65c4210fd993a2e1463121a83be831a4968915aae6c7bc0838950bb57dd799db4b8ce9758e7cecba1a132dbaa4a8a8e32b3be6bdc9d4e577a5cebff8a35f1dbf6aea697a4f02fd',

	/*
	|--------------------------------------------------------------------------
	| Username
	|--------------------------------------------------------------------------
	|
	| By default, it will use root as its username. If you have another username,
	| make sure it has the same privelege with the root or at least it can access
	| External API which is provided by CPanel/WHM
	|
	*/
	'username'=>'root',

];