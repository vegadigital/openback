<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accounts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('hostname');
            $table->string('plan');
            $table->string('account_type');
            $table->string('domain_name');
            $table->softDeletes();
            $table->timestamps();
        });
        Schema::create('account_user_role', function (Blueprint $table) {
            $table->integer('user_id')->index();
            $table->integer('account_id')->index();
            $table->integer('role_id')->index();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('accounts');
        Schema::drop('account_user_role');
    }
}
